// SPDX-License-Identifier: MIT
pragma solidity ^0.8.19;

import "@openzeppelin/contracts/proxy/Clones.sol";
import "./Summoner.sol";
import "./SpiritCat.sol";

contract Elaine is Summoner {
    SpiritCat public implementation;

    constructor() {
        implementation = new SpiritCat();
    }

    function summon(
        address recipient,
        IERC20 token,
        uint256 balance,
        uint256 lockTime
    ) external override {
        require(msg.sender == owner, "Not owner");
        SpiritCat cat = SpiritCat(Clones.clone(address(implementation)));
        cat.initialize(recipient, token, balance, lockTime);
        token.transfer(address(cat), balance);
        emit EscrowSummoned(address(cat));
    }

    function dispel(address escrow) external override {
        require(
            escrow != address(implementation),
            "Should not dispel master implementation"
        );
        require(msg.sender == owner, "Not owner");
        SpiritCat cat = SpiritCat(escrow);
        cat.dispel();
    }
}
