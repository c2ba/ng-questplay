// SPDX-License-Identifier: MIT
pragma solidity ^0.8.19;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/proxy/utils/Initializable.sol";
import "./TimeEscrow.sol";

contract SpiritCat is TimeEscrow, Initializable {
    address payable public summoner;
    uint lockTime;

    function initialize(
        address _recipient,
        IERC20 _token,
        uint256 _balance,
        uint256 _lockTime
    ) public initializer {
        summoner = payable(msg.sender);
        recipient = _recipient;
        isFullyLocked = false;
        token = _token;
        balance = _balance;
        lockTime = _lockTime;
        endTime = block.timestamp + _lockTime;
    }

    /**
     * @dev If {endTime} has not passed, transfers {unlockedBalance()}
     * amount of tokens to the recipient, and locks the remainder up.
     *
     * Otherwise, transfers all remaining tokens to the recipient.
     */
    function withdraw() external override {
        require(msg.sender == recipient, "Not recipient");
        require(balance > 0, "Already withdrawn");
        if (block.timestamp >= endTime) {
            balance = 0;
            token.transfer(recipient, token.balanceOf(address(this)));
            return;
        }
        require(isFullyLocked == false, "Locked");
        uint256 _unlockedBalance = unlockedBalance();
        isFullyLocked = true;
        balance -= _unlockedBalance;
        token.transfer(recipient, _unlockedBalance);
    }

    /**
     * @dev Transfers all remaining tokens to creator and self destructs.
     *
     * Fails if `msg.sender` is not creator of contract (i.e. {Summoner}).
     */
    function dispel() external override {
        require(msg.sender == summoner, "Not summoner");
        balance = 0;
        token.transfer(summoner, token.balanceOf(address(this)));
        selfdestruct(summoner);
    }

    /**
     * @dev Returns amount of tokens the recipient can withdraw early.
     *
     * Tokens are linearly unlocked over time and are
     * fully unlocked at {endTime}. If the recipient withdraws early,
     * all unwithdrawn tokens are locked until {endTime()}.
     */
    function unlockedBalance() public view override returns (uint256) {
        if (block.timestamp >= endTime) {
            return balance;
        }
        if (isFullyLocked) {
            return 0;
        }
        uint remainingTime = endTime - block.timestamp;
        uint ellapsedTime = lockTime - remainingTime;
        return (balance * ellapsedTime) / lockTime;
    }
}
