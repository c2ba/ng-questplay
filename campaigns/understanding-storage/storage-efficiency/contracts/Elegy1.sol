// SPDX-License-Identifier: MIT
pragma solidity ^0.8.19;

contract Elegy1 {
    bytes32 public secondVerse;
    bytes8 public firstVerse;
    uint128 public fourthVerse;
    address public thirdVerse;
    uint96 public fifthVerse;

    function setVerse(
        bytes8 _firstVerse,
        bytes32 _secondVerse,
        address _thirdVerse,
        uint128 _fourthVerse,
        uint96 _fifthVerse
    ) external {
        secondVerse = _secondVerse;
        firstVerse = _firstVerse;
        fourthVerse = _fourthVerse;
        thirdVerse = _thirdVerse;
        fifthVerse = _fifthVerse;
    }
}
