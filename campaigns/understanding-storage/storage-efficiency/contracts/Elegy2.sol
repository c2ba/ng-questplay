// SPDX-License-Identifier: MIT
pragma solidity ^0.8.19;

contract Elegy2 {
    uint32[] public lines;
    uint public totalSum;

    constructor(uint32[] memory _lines) {
        lines = _lines;
    }

    function play(uint nonce) external {
        uint totalSum_ = 0;
        uint length = lines.length;
        for (uint i = 0; i < length; i++) {
            totalSum_ += (i * nonce) * lines[i];
        }
        totalSum = totalSum_;
    }
}
