// SPDX-License-Identifier: MIT
pragma solidity ^0.8.19;

import "forge-std/Test.sol";
import "forge-std/console.sol";

contract Helper is Test {
    function testMapIndex() external {
        uint x = uint(keccak256(abi.encode(20, 2)));
        uint y = uint(keccak256(abi.encode(22, x)));
        console.log(y);
    }

    function testArrayIndex() external {
        uint x = uint(keccak256(abi.encode(3))) + 5;
        console.log(x);
    }
}
